#ifndef INCLUDE_DO_THREAD_H
#define INCLUDE_DO_THREAD_H

#include "thread_id.h"
#include <stdint.h>

#define THREAD_EX_REGS_TRAP_NUM 1
#define THREAD_EX_REGS_VECTOR_NUM (0x20 + THREAD_EX_REGS_TRAP_NUM)

static inline bool do_thread_ex_regs(thread_id id,
                                     uintptr_t pc, uintptr_t usp, uint8_t ccr,
                                     uintptr_t *old_pc, uintptr_t *old_usp, uint8_t *old_ccr) {
    register thread_id id_reg asm ("d0") = id;
    register uintptr_t pc_reg asm ("d1") = pc;
    register uintptr_t usp_reg asm("d2") = usp;
    register uint8_t ccr_reg asm("d3") = ccr;

    asm volatile (
        "trap    %[trap_num] \n"
        : "+r" (id_reg), "+r" (pc_reg), "+r" (usp_reg), "+r" (ccr_reg)
        : [trap_num] "Ci" (THREAD_EX_REGS_TRAP_NUM)
        : "d4", "d5", "d6", "d7", "a0", "a1", "a2", "a3", "a4", "a5", "cc", "memory"
    );

    if (id_reg == THREAD_ID_NIL) {
        return false;
    }

    if (old_pc) {
        *old_pc = pc_reg;
    }
    if (old_usp) {
        *old_usp = usp_reg;
    }
    if (old_ccr) {
        *old_ccr = ccr_reg;
    }

    return true;
}

#endif
