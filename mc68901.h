#ifndef INCLUDE_MC68901_H
#define INCLUDE_MC68901_H

#include "exception.h"
#include <machine.h>
#include <stdbool.h>
#include <stdint.h>

#define MC68901_USE_MOVEP 0

#define MC68901_R(reg) (*(volatile uint8_t *) (reg))

static inline unsigned long mc68901_xtal_freq_get(void) {
    return 3686400;
}

////////////////////////////////////////////////////////////////////////////////
// INTERRUPT STRUCTURE                                                        //
////////////////////////////////////////////////////////////////////////////////

enum mc68901_vr {
    MC68901_VR_V_MASK          = 0xF0,

    MC68901_VR_S               = 0x08, // In-Service Register Enable

    MC68901_VR_MASK            = 0xF8,
    MC68901_VR_MAX             = 0xF8,
};

enum mc68901_interrupt {
    MC68901_INTERRUPT_GPIP7            = 1 << 15,
    MC68901_INTERRUPT_GPIP6            = 1 << 14,
    MC68901_INTERRUPT_TIMER_A          = 1 << 13,
    MC68901_INTERRUPT_RCV_BUF_FULL     = 1 << 12,
    MC68901_INTERRUPT_RCV_ERR          = 1 << 11,
    MC68901_INTERRUPT_XMIT_BUF_EMPTY   = 1 << 10,
    MC68901_INTERRUPT_XMIT_ERR         = 1 << 9,
    MC68901_INTERRUPT_TIMER_B          = 1 << 8,
    MC68901_INTERRUPT_GPIP5            = 1 << 7,
    MC68901_INTERRUPT_GPIP4            = 1 << 6,
    MC68901_INTERRUPT_TIMER_C          = 1 << 5,
    MC68901_INTERRUPT_TIMER_D          = 1 << 4,
    MC68901_INTERRUPT_GPIP3            = 1 << 3,
    MC68901_INTERRUPT_GPIP2            = 1 << 2,
    MC68901_INTERRUPT_GPIP1            = 1 << 1,
    MC68901_INTERRUPT_GPIP0            = 1 << 0,

    MC68901_INTERRUPT_MASK             = 0xFFFF,
    MC68901_INTERRUPT_MAX              = 0xFFFF,
};

static inline uint8_t mc68901_vr_interrupt_vector_number_get(void) {
    return MC68901_R(MFP_VR) & MC68901_VR_V_MASK;
}

static inline void mc68901_vr_interrupt_vector_number_set(uint8_t int_vect_num_upper_4) {
    MC68901_R(MFP_VR) = (int_vect_num_upper_4 & MC68901_VR_V_MASK) | (MC68901_R(MFP_VR) & ~MC68901_VR_V_MASK);
}

static inline bool mc68901_vr_in_service_enable_get(void) {
    return MC68901_R(MFP_VR) & MC68901_VR_S;
}

static inline void mc68901_vr_in_service_enable_set(bool enable) {
    MC68901_R(MFP_VR) = (enable ? MC68901_VR_S : 0) | (MC68901_R(MFP_VR) & ~MC68901_VR_S);
}

static inline enum mc68901_interrupt mc68901_ier_get(void) {
    enum mc68901_interrupt val = 0;
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IERA)
    );
#else
    val = MC68901_R(MFP_IERA) << 8 | MC68901_R(MFP_IERB);
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_ier_set(enum mc68901_interrupt val) {
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_IERA), [val] "d" (val)
        : "memory"
    );
#else
    MC68901_R(MFP_IERA) = val >> 8;
    MC68901_R(MFP_IERB) = val;
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_ier_bitset(enum mc68901_interrupt mask) {
    enum mc68901_interrupt val = mc68901_ier_get();
    val |= mask;
    mc68901_ier_set(val);
    return val;
}

static inline enum mc68901_interrupt mc68901_ier_bitclear(enum mc68901_interrupt mask) {
    enum mc68901_interrupt val = mc68901_ier_get();
    val &= ~mask;
    mc68901_ier_set(val);
    return val;
}

static inline enum mc68901_interrupt mc68901_ipr_get(void) {
    enum mc68901_interrupt val = 0;
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IPRA)
    );
#else
    val = MC68901_R(MFP_IPRA) << 8 | MC68901_R(MFP_IPRB);
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_ipr_set(enum mc68901_interrupt val) {
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_IPRA), [val] "d" (val)
        : "memory"
    );
#else
    MC68901_R(MFP_IPRA) = val >> 8;
    MC68901_R(MFP_IPRB) = val;
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_ipr_bitclear(enum mc68901_interrupt mask) {
    enum mc68901_interrupt val = mc68901_ipr_get();
    val &= ~mask;
    mc68901_ipr_set(val);
    return val;
}

static inline enum mc68901_interrupt mc68901_isr_get(void) {
    enum mc68901_interrupt val = 0;
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IPRA)
    );
#else
    val = MC68901_R(MFP_ISRA) << 8 | MC68901_R(MFP_ISRB);
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_isr_set(enum mc68901_interrupt val) {
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_ISRA), [val] "d" (val)
        : "memory"
    );
#else
    MC68901_R(MFP_ISRA) = val >> 8;
    MC68901_R(MFP_ISRB) = val;
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_isr_bitclear(enum mc68901_interrupt mask) {
    enum mc68901_interrupt val = mc68901_isr_get();
    val &= ~mask;
    mc68901_isr_set(val);
    return val;
}

static inline enum mc68901_interrupt mc68901_imr_get(void) {
    enum mc68901_interrupt val = 0;
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw (0,%[reg]), %[val] \n"
        : [val] "+d" (val)
        : [reg] "a" (MFP_IPRA)
    );
#else
    val = MC68901_R(MFP_IMRA) << 8 | MC68901_R(MFP_IMRB);
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_imr_set(enum mc68901_interrupt val) {
#if MC68901_USE_MOVEP
    asm volatile (
        "movepw %[val], (0,%[reg]) \n"
        :
        : [reg] "a" (MFP_IMRA), [val] "d" (val)
        : "memory"
    );
#else
    MC68901_R(MFP_IMRA) = val >> 8;
    MC68901_R(MFP_IMRB) = val;
#endif
    return val;
}

static inline enum mc68901_interrupt mc68901_imr_bitset(enum mc68901_interrupt mask) {
    enum mc68901_interrupt val = mc68901_imr_get();
    val |= mask;
    mc68901_imr_set(val);
    return val;
}

static inline enum mc68901_interrupt mc68901_imr_bitclear(enum mc68901_interrupt mask) {
    enum mc68901_interrupt val = mc68901_imr_get();
    val &= ~mask;
    mc68901_imr_set(val);
    return val;
}

static inline void mc68901_interrupt_install(exception_raw_func handler, enum mc68901_interrupt vectors) {
    uint8_t vector_base = mc68901_vr_interrupt_vector_number_get();
    for (unsigned i = 0; i < 16 && vectors; ++i) {
        if (vectors & (1U << i)) {
            exception_vector_raw_replace(handler, vector_base + i);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// GENERAL PURPOSE INPUT/OUTPUT INTERRUPT PORT                                //
////////////////////////////////////////////////////////////////////////////////

static inline uint8_t mc68901_gpdr_get(void) {
    return MC68901_R(MFP_GPDR);
}

static inline void mc68901_gpdr_set(uint8_t gpdr) {
    MC68901_R(MFP_GPDR) = gpdr;
}

static inline void mc68901_gpdr_set_masked(uint8_t gpdr, uint8_t mask) {
    MC68901_R(MFP_GPDR) = (MC68901_R(MFP_GPDR) & ~mask) | (gpdr & mask);
}

static inline uint8_t mc68901_aer_get(void) {
    return MC68901_R(MFP_AER);
}

static inline void mc68901_aer_set(uint8_t aer) {
    MC68901_R(MFP_AER) = aer;
}

static inline void mc68901_aer_set_masked(uint8_t aer, uint8_t mask) {
    MC68901_R(MFP_AER) = (MC68901_R(MFP_AER) & ~mask) | (aer & mask);
}

static inline uint8_t mc68901_ddr_get(void) {
    return MC68901_R(MFP_DDR);
}

static inline void mc68901_ddr_set(uint8_t ddr) {
    MC68901_R(MFP_DDR) = ddr;
}

static inline void mc68901_ddr_set_masked(uint8_t ddr, uint8_t mask) {
    MC68901_R(MFP_DDR) = (MC68901_R(MFP_DDR) & ~mask) | (ddr & mask);
}

static inline void mc68901_gpip_config(uint8_t gpdr, uint8_t aer, uint8_t ddr) {
    MC68901_R(MFP_DDR) &= ddr;
    MC68901_R(MFP_AER) = aer;
    MC68901_R(MFP_GPDR) = gpdr;
    MC68901_R(MFP_DDR) |= ddr;
}

static inline void mc68901_gpip_config_masked(uint8_t gpdr, uint8_t aer, uint8_t ddr, uint8_t mask) {
    MC68901_R(MFP_DDR) &= (~mask) | (ddr & mask);
    MC68901_R(MFP_AER) = (MC68901_R(MFP_AER) & ~mask) | (aer & mask);
    MC68901_R(MFP_GPDR) = (MC68901_R(MFP_GPDR) & ~mask) | (gpdr & mask);
    MC68901_R(MFP_DDR) |= (ddr & mask);
}

////////////////////////////////////////////////////////////////////////////////
// TIMERS                                                                     //
////////////////////////////////////////////////////////////////////////////////

enum mc68901_tacr {
    MC68901_TACR_AC_STOPPED    = 0x00,     // A Control: Timer Stopped
    MC68901_TACR_AC_DELAY_4    = 0x01,     // A Control: Delay Mode, /4 Prescaler
    MC68901_TACR_AC_DELAY_10   = 0x02,     // A Control: Delay Mode, /10 Prescaler
    MC68901_TACR_AC_DELAY_16   = 0x03,     // A Control: Delay Mode, /16 Prescaler
    MC68901_TACR_AC_DELAY_50   = 0x04,     // A Control: Delay Mode, /50 Prescaler
    MC68901_TACR_AC_DELAY_64   = 0x05,     // A Control: Delay Mode, /64 Prescaler
    MC68901_TACR_AC_DELAY_100  = 0x06,     // A Control: Delay Mode, /100 Prescaler
    MC68901_TACR_AC_DELAY_200  = 0x07,     // A Control: Delay Mode, /200 Prescaler
    MC68901_TACR_AC_EVENT      = 0x08,     // A Control: Event Count Mode
    MC68901_TACR_AC_PULSE_4    = 0x09,     // A Control: Pulse Width Mode, /4 Prescaler
    MC68901_TACR_AC_PULSE_10   = 0x0A,     // A Control: Pulse Width Mode, /10 Prescaler
    MC68901_TACR_AC_PULSE_16   = 0x0B,     // A Control: Pulse Width Mode, /16 Prescaler
    MC68901_TACR_AC_PULSE_50   = 0x0C,     // A Control: Pulse Width Mode, /50 Prescaler
    MC68901_TACR_AC_PULSE_64   = 0x0D,     // A Control: Pulse Width Mode, /64 Prescaler
    MC68901_TACR_AC_PULSE_100  = 0x0E,     // A Control: Pulse Width Mode, /100 Prescaler
    MC68901_TACR_AC_PULSE_200  = 0x0F,     // A Control: Pulse Width Mode, /200 Prescaler
    MC68901_TACR_AC_MASK       = 0x0F,

    MC68901_TACR_RESET_TAO     = 0x10,     // Reset TAO

    MC68901_TACR_MASK          = 0x1F,
    MC68901_TACR_MAX           = 0x1F,
};

enum mc68901_tbcr {
    MC68901_TBCR_BC_STOPPED    = 0x00,     // B Control: Timer Stopped
    MC68901_TBCR_BC_DELAY_4    = 0x01,     // B Control: Delay Mode, /4 Prescaler
    MC68901_TBCR_BC_DELAY_10   = 0x02,     // B Control: Delay Mode, /10 Prescaler
    MC68901_TBCR_BC_DELAY_16   = 0x03,     // B Control: Delay Mode, /16 Prescaler
    MC68901_TBCR_BC_DELAY_50   = 0x04,     // B Control: Delay Mode, /50 Prescaler
    MC68901_TBCR_BC_DELAY_64   = 0x05,     // B Control: Delay Mode, /64 Prescaler
    MC68901_TBCR_BC_DELAY_100  = 0x06,     // B Control: Delay Mode, /100 Prescaler
    MC68901_TBCR_BC_DELAY_200  = 0x07,     // B Control: Delay Mode, /200 Prescaler
    MC68901_TBCR_BC_EVENT      = 0x08,     // B Control: Event Count Mode
    MC68901_TBCR_BC_PULSE_4    = 0x09,     // B Control: Pulse Width Mode, /4 Prescaler
    MC68901_TBCR_BC_PULSE_10   = 0x0A,     // B Control: Pulse Width Mode, /10 Prescaler
    MC68901_TBCR_BC_PULSE_16   = 0x0B,     // B Control: Pulse Width Mode, /16 Prescaler
    MC68901_TBCR_BC_PULSE_50   = 0x0C,     // B Control: Pulse Width Mode, /50 Prescaler
    MC68901_TBCR_BC_PULSE_64   = 0x0D,     // B Control: Pulse Width Mode, /64 Prescaler
    MC68901_TBCR_BC_PULSE_100  = 0x0E,     // B Control: Pulse Width Mode, /100 Prescaler
    MC68901_TBCR_BC_PULSE_200  = 0x0F,     // B Control: Pulse Width Mode, /200 Prescaler
    MC68901_TBCR_BC_MASK       = 0x0F,

    MC68901_TBCR_RESET_TBO     = 0x10,     // Reset TBO

    MC68901_TBCR_MASK          = 0x1F,
    MC68901_TBCR_MAX           = 0x1F,
};

enum mc68901_tcdcr {
    MC68901_TCDCR_CC_STOPPED   = 0x0 << 4, // C Control: Timer Stopped
    MC68901_TCDCR_CC_DELAY_4   = 0x1 << 4, // C Control: Delay Mode, /4 Prescaler
    MC68901_TCDCR_CC_DELAY_10  = 0x2 << 4, // C Control: Delay Mode, /10 Prescaler
    MC68901_TCDCR_CC_DELAY_16  = 0x3 << 4, // C Control: Delay Mode, /16 Prescaler
    MC68901_TCDCR_CC_DELAY_50  = 0x4 << 4, // C Control: Delay Mode, /50 Prescaler
    MC68901_TCDCR_CC_DELAY_64  = 0x5 << 4, // C Control: Delay Mode, /64 Prescaler
    MC68901_TCDCR_CC_DELAY_100 = 0x6 << 4, // C Control: Delay Mode, /100 Prescaler
    MC68901_TCDCR_CC_DELAY_200 = 0x7 << 4, // C Control: Delay Mode, /200 Prescaler
    MC68901_TCDCR_CC_MASK      = 0x7 << 4,

    MC68901_TCDCR_DC_STOPPED   = 0x0 << 0, // D Control: Timer Stopped
    MC68901_TCDCR_DC_DELAY_4   = 0x1 << 0, // D Control: Delay Mode, /4 Prescaler
    MC68901_TCDCR_DC_DELAY_10  = 0x2 << 0, // D Control: Delay Mode, /10 Prescaler
    MC68901_TCDCR_DC_DELAY_16  = 0x3 << 0, // D Control: Delay Mode, /16 Prescaler
    MC68901_TCDCR_DC_DELAY_50  = 0x4 << 0, // D Control: Delay Mode, /50 Prescaler
    MC68901_TCDCR_DC_DELAY_64  = 0x5 << 0, // D Control: Delay Mode, /64 Prescaler
    MC68901_TCDCR_DC_DELAY_100 = 0x6 << 0, // D Control: Delay Mode, /100 Prescaler
    MC68901_TCDCR_DC_DELAY_200 = 0x7 << 0, // D Control: Delay Mode, /200 Prescaler
    MC68901_TCDCR_DC_MASK      = 0x7 << 0,

    MC68901_TCDCR_MASK         = 0x77,
    MC68901_TCDCR_MAX          = 0x77,
};

static inline void mc68901_tacr_reset_tao(void) {
    MC68901_R(MFP_TACR) |= MC68901_TACR_RESET_TAO;
}

static inline enum mc68901_tacr mc68901_tacr_ac_get(void) {
    return MC68901_R(MFP_TACR) & MC68901_TACR_AC_MASK;
}

static inline void mc68901_tacr_ac_set(enum mc68901_tacr tacr) {
    MC68901_R(MFP_TACR) = (MC68901_R(MFP_TACR) & ~MC68901_TACR_AC_MASK) | (tacr & MC68901_TACR_AC_MASK);
}

static inline void mc68901_tbcr_reset_tbo(void) {
    MC68901_R(MFP_TBCR) |= MC68901_TBCR_RESET_TBO;
}

static inline enum mc68901_tbcr mc68901_tbcr_bc_get(void) {
    return MC68901_R(MFP_TBCR) & MC68901_TBCR_BC_MASK;
}

static inline void mc68901_tbcr_bc_set(enum mc68901_tbcr tbcr) {
    MC68901_R(MFP_TBCR) = (MC68901_R(MFP_TBCR) & ~MC68901_TBCR_BC_MASK) | (tbcr & MC68901_TBCR_BC_MASK);
}

static inline enum mc68901_tcdcr mc68901_tcdcr_cc_get(void) {
    return MC68901_R(MFP_TCDCR) & MC68901_TCDCR_CC_MASK;
}

static inline void mc68901_tcdcr_cc_set(enum mc68901_tcdcr tcdcr) {
    MC68901_R(MFP_TCDCR) = (MC68901_R(MFP_TCDCR) & ~MC68901_TCDCR_CC_MASK) | (tcdcr & MC68901_TCDCR_CC_MASK);
}

static inline enum mc68901_tcdcr mc68901_tcdcr_dc_get(void) {
    return MC68901_R(MFP_TCDCR) & MC68901_TCDCR_DC_MASK;
}

static inline void mc68901_tcdcr_dc_set(enum mc68901_tcdcr tcdcr) {
    MC68901_R(MFP_TCDCR) = (MC68901_R(MFP_TCDCR) & ~MC68901_TCDCR_DC_MASK) | (tcdcr & MC68901_TCDCR_DC_MASK);
}

static inline uint8_t mc68901_tadr_get(void) {
    return MC68901_R(MFP_TADR);
}

static inline void mc68901_tadr_set(uint8_t tadr) {
    MC68901_R(MFP_TADR) = tadr;
}

static inline uint8_t mc68901_tbdr_get(void) {
    return MC68901_R(MFP_TBDR);
}

static inline void mc68901_tbdr_set(uint8_t tbdr) {
    MC68901_R(MFP_TBDR) = tbdr;
}

static inline uint8_t mc68901_tcdr_get(void) {
    return MC68901_R(MFP_TCDR);
}

static inline void mc68901_tcdr_set(uint8_t tcdr) {
    MC68901_R(MFP_TCDR) = tcdr;
}

static inline uint8_t mc68901_tddr_get(void) {
    return MC68901_R(MFP_TDDR);
}

static inline void mc68901_tddr_set(uint8_t tddr) {
    MC68901_R(MFP_TDDR) = tddr;
}

////////////////////////////////////////////////////////////////////////////////
// UNIVERSAL SYNCHRONOUS/ASYNCHRONOUS RECEIVER-TRANSMITTER                    //
////////////////////////////////////////////////////////////////////////////////

enum mc68901_ucr {
    MC68901_UCR_CLK_1              = 0 << 7,   // Clock Mode: Divide-by-1
    MC68901_UCR_CLK_16             = 1 << 7,   // Clock Mode: Divide-by-16
    MC68901_UCR_CLK_MASK           = 1 << 7,

    MC68901_UCR_WL_8               = 0 << 5,   // Word Length: 8 Bits
    MC68901_UCR_WL_7               = 1 << 5,   // Word Length: 7 Bits
    MC68901_UCR_WL_6               = 2 << 5,   // Word Length: 6 Bits
    MC68901_UCR_WL_5               = 3 << 5,   // Word Length: 5 Bits
    MC68901_UCR_WL_MASK            = 3 << 5,

    MC68901_UCR_ST_0_0__SYNC       = 0 << 3,   // Start/Stop Bit and Format Control: 0 Start, 0 Stop, Synchronous
    MC68901_UCR_ST_1_1__ASYNC      = 1 << 3,   // Start/Stop Bit and Format Control: 1 Start, 1 Stop, Asynchronous
    MC68901_UCR_ST_1_15_ASYNC      = 2 << 3,   // Start/Stop Bit and Format Control: 1 Start, 1.5 Stop, Asynchronous
    MC68901_UCR_ST_1_2__ASYNC      = 3 << 3,   // Start/Stop Bit and Format Control: 1 Start, 2 Stop, Asynchronous
    MC68901_UCR_ST_MASK            = 3 << 3,

    MC68901_UCR_PE                 = 1 << 2,   // Parity Enable

    MC68901_UCR_EO_ODD             = 0 << 1,   // Even/Odd Parity: Odd
    MC68901_UCR_EO_EVEN            = 1 << 1,   // Even/Odd Parity: Even
    MC68901_UCR_EO_MASK            = 1 << 1,

    MC68901_UCR_MASK               = 0xFE,
    MC68901_UCR_MAX                = 0xFE,
};

enum mc68901_rsr {
    MC68901_RSR_BF                 = 1 << 7,   // Buffer Full
    MC68901_RSR_OE                 = 1 << 6,   // Overrun Error
    MC68901_RSR_PE                 = 1 << 5,   // Parity Error
    MC68901_RSR_FE                 = 1 << 4,   // Frame Error
    MC68901_RSR_FS                 = 1 << 3,   // Found/Search
    MC68901_RSR_B                  = 1 << 3,   // Break Detect
    MC68901_RSR_M                  = 1 << 2,   // Match
    MC68901_RSR_CIP                = 1 << 2,   // Character in Progress
    MC68901_RSR_SS                 = 1 << 1,   // Synchronous Strip Enable
    MC68901_RSR_RE                 = 1 << 0,   // Receiver Enable

    MC68901_RSR_MASK               = 0xFF,
    MC68901_RSR_MAX                = 0xFF,
};

enum mc68901_tsr {
    MC68901_TSR_BE                 = 1 << 7,   // Buffer Empty
    MC68901_TSR_UE                 = 1 << 6,   // Underrun Error
    MC68901_TSR_AT                 = 1 << 5,   // Auto-Turnaround
    MC68901_TSR_END                = 1 << 4,   // End of Transmission
    MC68901_TSR_B                  = 1 << 3,   // Break

    MC68901_TSR_HL_HIGH_IMPEDANCE  = 0 << 1,   // High and Low: High Impedance
    MC68901_TSR_HL_LOW             = 1 << 1,   // High and Low: Low
    MC68901_TSR_HL_HIGH            = 2 << 1,   // High and Low: High
    MC68901_TSR_HL_LOOPBACK_MODE   = 3 << 1,   // High and Low: Loopback Mode
    MC68901_TSR_HL_MASK            = 3 << 1,

    MC68901_TSR_TE                 = 1 << 0,   // Transmitter Enable

    MC68901_TSR_MASK               = 0xFF,
    MC68901_TSR_MAX                = 0xFF,
};

static inline uint8_t mc68901_scr_get(void) {
    return MC68901_R(MFP_SCR);
}

static inline void mc68901_scr_set(uint8_t scr) {
    MC68901_R(MFP_SCR) = scr;
}

static inline enum mc68901_ucr mc68901_ucr_get(void) {
    return MC68901_R(MFP_UCR);
}

static inline void mc68901_ucr_set(enum mc68901_ucr ucr) {
    MC68901_R(MFP_UCR) = ucr;
}

static inline enum mc68901_rsr mc68901_rsr_get() {
    return MC68901_R(MFP_RSR);
}

static inline void mc68901_rsr_set(enum mc68901_rsr rsr) {
    MC68901_R(MFP_RSR) = rsr;
}

static inline enum mc68901_tsr mc68901_tsr_get() {
    return MC68901_R(MFP_TSR);
}

static inline void mc68901_tsr_set(enum mc68901_tsr tsr) {
    MC68901_R(MFP_TSR) = tsr;
}

static inline uint8_t mc68901_udr_get() {
    return MC68901_R(MFP_UDR);
}

static inline void mc68901_udr_set(uint8_t udr) {
    MC68901_R(MFP_UDR) = udr;
}

#undef MC68901_R

#endif
