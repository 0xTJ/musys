#include "ipc.h"
#include "mem.h"
#include <stdio.h>

#define IPC_INFO_ASSERT_FIELD(field, FIELD, value) \
    do { \
        ipc_info ipc_info_assert_field_tmp = { .field = ((value) & IPC_ ## FIELD ## _VMASK) }; \
        assert(ipc_info_assert_field_tmp.raw == (((ipc_info_raw) (value) << IPC_ ## FIELD ## _SHIFT) & IPC_ ## FIELD ## _MASK)); \
    } while (0)

#define IPC_INFO_ASSERT_FIELD_BATTERY(field, FIELD) \
    do { \
        IPC_INFO_ASSERT_FIELD(field, FIELD, 0); \
        IPC_INFO_ASSERT_FIELD(field, FIELD, 0xFFFFFFFF); \
        IPC_INFO_ASSERT_FIELD(field, FIELD, 1); \
        IPC_INFO_ASSERT_FIELD(field, FIELD, (ipc_info_raw) 1 << (IPC_ ## FIELD ## _WIDTH - 1)); \
        IPC_INFO_ASSERT_FIELD(field, FIELD, 0x01234567); \
        IPC_INFO_ASSERT_FIELD(field, FIELD, 0x76543210); \
        IPC_INFO_ASSERT_FIELD(field, FIELD, 0x89ABCDEF); \
        IPC_INFO_ASSERT_FIELD(field, FIELD, 0xFEDCBA98); \
    } while (0)

thread_id ipc_exception_handlers[EXCEPTION_COUNT] = {0};

static thread *ipc_find_sender_for_id(thread_id send_dest);
static void ipc_send(struct exception_state *state, thread_id current_thread_id, thread *send_dest);
static void ipc_recv(struct exception_state *state, thread_id current_thread_id, thread *recv_src);

void ipc_assert_layout(void) {
    IPC_INFO_ASSERT_FIELD_BATTERY(id, ID);
    IPC_INFO_ASSERT_FIELD_BATTERY(block, BLOCK);
    IPC_INFO_ASSERT_FIELD_BATTERY(has_string, HAS_STRING);
    IPC_INFO_ASSERT_FIELD_BATTERY(padding, PADDING);
    IPC_INFO_ASSERT_FIELD_BATTERY(error, ERROR);
}

void ipc_strings_copy(thread *dest_thrd, ipc_string *dest_strings, thread *src_thrd, ipc_string *src_strings) {
    size_t strings_done = 0;
    if (dest_strings && src_strings) while (strings_done < IPC_STRINGS_MAX) {
        opt_ptr dest_addr_opt = mem_get_ptr(dest_thrd, &dest_strings->addr);
        opt_ptr src_addr_opt = mem_get_ptr(src_thrd, &src_strings->addr);
        if (!dest_addr_opt.exists || !src_addr_opt.exists) {
            break;
        }
        void *dest_addr = dest_addr_opt.val;
        void *src_addr = src_addr_opt.val;
        if (!dest_addr || !src_addr) {
            break;
        }
        
        opt_size_t dest_size_opt = mem_get_size_t(dest_thrd, &dest_strings->size);
        opt_size_t src_size_opt = mem_get_size_t(src_thrd, &src_strings->size);
        if (!dest_size_opt.exists || !src_size_opt.exists) {
            break;
        }
        size_t dest_size = dest_size_opt.val;
        size_t src_size = src_size_opt.val;

        size_t size = src_size;
        if (dest_size < size) {
            size = dest_size;
        }

        mem_copy(dest_thrd, dest_addr, src_thrd, src_addr, size);

        mem_set_size_t(dest_thrd, &dest_strings->size, dest_size - size);
        mem_set_size_t(src_thrd, &src_strings->size, src_size - size);

        dest_strings += 1;
        src_strings += 1;
        strings_done += 1;
    }
}

inline bool ipc_can_send(thread *thrd) {
    return thrd->send.block;
}

inline bool ipc_can_recv(thread *thrd) {
    return !thrd->send.block && thrd->recv.block;
}

static thread *ipc_find_sender_for_id(thread_id send_dest) {
    for (thread_id_index i = 0; i < THREAD_COUNT; ++i) {
        if (ipc_can_send(&thread_table[i]) && thread_id_matches(send_dest, thread_table[i].send.id)) {
            // Found a thread looking to send to this thread
            return &thread_table[i];
        }
    }
    return NULL;
}

static void ipc_send(struct exception_state *state, thread_id current_thread_id, thread *send_dest) {
    if (ipc_can_recv(send_dest) && thread_id_matches(current_thread_id, send_dest->recv.id)) {
        // Can send to destination thread immediately
        thread_current->send.id = current_thread_id;
        thread_current->send.block = false;
        send_dest->recv.id = current_thread_id;
        send_dest->recv.block = false;
        
        ipc_strings_copy(send_dest, (void *) send_dest->saved_state.a[3], thread_current, (void *) state->a[2]);
        thread_hot_switch(send_dest, state);
    } else if (thread_current->send.block) {
        // Block and wait for receiving thread
        thread_schedule(state);
    } else {
        // Non-blocking send with destination not waiting is an error
        thread_current->send.error = true;
    }
}

static void ipc_recv(struct exception_state *state, thread_id current_thread_id, thread *recv_src) {
    if (ipc_can_send(recv_src) && thread_id_matches(current_thread_id, recv_src->send.id)) {
        // Can receive from source thread immediately
        bool previous_recv_src_send_block = recv_src->send.block;
        thread_current->recv.id = current_thread_id;
        thread_current->recv.block = false;
        recv_src->send.id = current_thread_id;
        recv_src->send.block = false;

        ipc_strings_copy(thread_current, (void *) state->a[3], recv_src, (void *) recv_src->saved_state.a[2]);
        thread_copy_data(state, &recv_src->saved_state);

        if (previous_recv_src_send_block && recv_src->recv.block) {
            // Perform send for a blocked thread waiting to send to recv_src->recv, if any
            // TODO: Implement this. The previous version was broken.
            // for (thread_id_index i = 0; i < THREAD_COUNT; ++i) {
            //     if (thread_table[i].send.block &&
            //         thread_id_matches(thread_current->recv.id, thread_table[i].send.id) &&
            //         thread_id_matches(thread_to_id(&thread_table[i]), recv_src->recv.id)) {
            //         thread_copy_data(&recv_src->saved_state, &thread_table[i].saved_state);
            //         recv_src->recv.block = false;
            //         break;
            //     }
            // }
        }
    } else if (thread_current->recv.block) {
        // Block and wait for sending thread
        thread_schedule(state);
    } else {
        // Non-blocking receive with source not waiting is an error
        thread_current->recv.error = true;
    }
}

void ipc_force_unblock(void) {
    if (thread_current->send.block) {
        thread_current->send.error = true;
        thread_current->send.block = false;
    }
    if (thread_current->recv.block) {
        thread_current->recv.error = true;
        thread_current->recv.block = false;
    }
}

void ipc_cleanup(struct exception_state *state) {
    assert(!thread_current->send.block);
    assert(!thread_current->recv.block);

    if (thread_current->send_raw != 0) {
        state->d[0] = thread_current->send_raw;
        thread_current->send_raw = 0;
    }
    
    if (thread_current->recv_raw != 0) {
        state->d[1] = thread_current->recv_raw;
        thread_current->recv_raw = 0;
    }
}

void ipc(struct exception_state *state) {
    asm ("ori #(7 << 8), %SR");

    thread_current->send_raw = state->d[0];
    thread_current->recv_raw = state->d[1];

    // printf("IPC %08lX %08lX\n", thread_current->send_raw, thread_current->recv_raw);

    thread_id current_thread_id = thread_to_id(thread_current);

    if (thread_current->send.id != THREAD_ID_NIL) {
        thread *send_dest = NULL;

        // Can't send to wildcard address, so don't handle as special case
        
        if (!send_dest) {
            send_dest = thread_from_id(thread_current->send.id);
        }

        if (send_dest) {
            ipc_send(state, current_thread_id, send_dest);
        } else {
            thread_current->send.error = true;
        }

        goto done_ipc;
    } else if (thread_current->send_raw != 0) {
        thread_current->send.block = false;
        thread_current->send.error = true;
    }

    assert(!thread_current->send.block);

    if (thread_current->recv.id != THREAD_ID_NIL) {
        struct thread *recv_src = NULL;

        if (thread_id_matches(THREAD_ID_WILD, thread_current->recv.id)) {
            recv_src = ipc_find_sender_for_id(current_thread_id);
        }

        if (!recv_src && thread_id_matches(THREAD_ID_EXCP, thread_current->recv.id)) {
            // A not-yet-handled wildcard receive, or receive from interrupt is a block
            thread_schedule(state);
            goto done_ipc;
        }

        if (!recv_src) {
            recv_src = thread_from_id(thread_current->recv.id);
        }

        if (recv_src) {
            ipc_recv(state, current_thread_id, recv_src);
        } else {
            thread_current->recv.error = true;
        }

        goto done_ipc;
    } else if (thread_current->recv_raw != 0) {
        thread_current->recv.block = false;
        thread_current->recv.error = true;
    }

    assert(!thread_current->recv.block);

done_ipc:
    ipc_cleanup(state);
}

void ipc_exception(struct exception_state *state) {
    asm ("ori #(7 << 8), %SR");
    ipc_deliver_exception(state);
}

// TODO: Does this safely handle long-format frames?

bool ipc_deliver_exception(struct exception_state *state) {
    uint8_t vector_number = exception_vector_number_get(state);
    assert(vector_number < sizeof(ipc_exception_handlers) / sizeof(ipc_exception_handlers[0]));
    thread_id exception_handler_id;
    bool is_code_caused;

    // TODO: Handle edge cases
    // TODO: Missed exceptions should be stored to minimize drops

    switch (vector_number) {
    case EXCEPTION_NUM_BUS_ERROR:
    case EXCEPTION_NUM_ADDRESS_ERROR:
    case EXCEPTION_NUM_ILLEGAL_INSTRUCTION:
    case EXCEPTION_NUM_ZERO_DIVIDE:
    case EXCEPTION_NUM_CHK_INSTRUCTION:
    case EXCEPTION_NUM_TRAPV_INSTRUCTION:
    case EXCEPTION_NUM_PRIVILEGE_VIOLATION:
    // case EXCEPTION_NUM_TRACE:
        exception_handler_id = thread_current->exceptor;
        is_code_caused = true;
        break;
    default:
        exception_handler_id = ipc_exception_handlers[vector_number];
        is_code_caused = false;
        break;
    }

    if (is_code_caused) {
        assert(!(state->sr & (1U << 13)));
    }

    thread *send_dest = thread_from_id(exception_handler_id);
    if (!send_dest) {
        thread_id_index lowest_index;
        assert(thread_get_first(&lowest_index));
        send_dest = thread_from_index(lowest_index);
        exception_handler_id = thread_to_id(send_dest);
    }

    // TODO: Handle case of thread handling its own exception

    if (is_code_caused) {
        thread_current->recv_raw = 0;
        thread_current->recv.id = exception_handler_id;
        thread_current->recv.block = true;
    }

    assert(send_dest);

    if (ipc_can_recv(send_dest) && thread_id_matches(THREAD_ID_EXCP, send_dest->recv.id)) {
        // Can send to destination thread immediately
        send_dest->saved_state.d[2] = vector_number;
        if (is_code_caused) {
            send_dest->saved_state.d[3] = thread_to_id(thread_current);
        } else {
            send_dest->saved_state.d[3] = THREAD_ID_NIL;
        }
        send_dest->recv.id = THREAD_ID_EXCP; // Overwrite in case it's currently THREAD_ID_WILD
        send_dest->recv.block = false;
        thread_schedule(state);
        return true;
    } else {
        printf("Thread not ready to handle exception 0x%hhX\n", vector_number);
        return false;
    }
}
