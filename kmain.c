#include "do_ipc.h"
#include "do_thread.h"
#include "exception.h"
#include "thread.h"
#include "assert.h" // TODO: Make <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "mc68901.h"

char sts[1024];
char st1[1024];
char stl[1024];

void enter1();

void sigma() {
    uint32_t data[8] = {0};
    ipc_info_pair res;
    res = do_ipc_reply_and_wait(THREAD_ID_NIL, false, data, NULL, NULL);
    bool skip_this = false;
    do_thread_ex_regs(0x101, (uintptr_t) enter1, (uintptr_t) (st1 + 1024), 0, NULL, NULL, NULL);

    char buf[100] = "hello";
    ipc_string send_string[] = {
        { buf, strlen(buf) },
        { NULL, 0 }
    };
    do_ipc_send(0x101, true, data, send_string);

    while (1) {
        if (res.recv.error) {
            // printf("err\n");
            continue;
        }
        if (res.recv.id == THREAD_ID_EXCP) {
            if (data[1] == THREAD_ID_NIL) {
                if (data[0] == 0x45) {
                    printf("G");
                    mc68901_isr_bitclear(MC68901_INTERRUPT_TIMER_C);
                }
            } else {
                printf("Caught exception 0x%02lX from thread 0x%04lX\n", data[0], data[1]);
            }
            skip_this = true;
        }

        if (skip_this) {
            res = do_ipc_generic(THREAD_ID_NIL, false, THREAD_ID_EXCP, true, data, NULL, NULL);
        } else {
            res = do_ipc_generic(0x101, false, THREAD_ID_EXCP, true, data, NULL, NULL);
        }
        skip_this = false;
    }
}

void enter1() {
    uint32_t data[8] = {0};
    while (true) {
        char buf[100];
        ipc_string recv_string[] = {
            { buf, sizeof(buf) },
            { NULL, 0 }
        };
        do_ipc_wait(data, recv_string);
        printf("got: %s\n", buf);
    }
}

void idle() {
    while (true) {
        // Loop forever
    }
}

void kmain() {
    ipc_assert_layout();

    exception_init();
    exception_vector_replace(ipc, IPC_VECTOR_NUM);
    exception_vector_replace(thread_ex_regs, THREAD_EX_REGS_VECTOR_NUM);

    thread_id sigma_id = thread_create_first(sigma, sts + 1024, THREAD_ID_NIL, &thread_current);
    thread_create_first(NULL, NULL, sigma_id, NULL);
    thread_create_last(idle, stl + 1024, sigma_id, NULL);
    
    exception_vector_replace(ipc_exception, EXCEPTION_NUM_BUS_ERROR);
    exception_vector_replace(ipc_exception, EXCEPTION_NUM_ADDRESS_ERROR);
    exception_vector_replace(ipc_exception, EXCEPTION_NUM_ILLEGAL_INSTRUCTION);
    exception_vector_replace(ipc_exception, EXCEPTION_NUM_ZERO_DIVIDE);
    exception_vector_replace(ipc_exception, EXCEPTION_NUM_CHK_INSTRUCTION);
    exception_vector_replace(ipc_exception, EXCEPTION_NUM_TRAPV_INSTRUCTION);
    exception_vector_replace(ipc_exception, EXCEPTION_NUM_PRIVILEGE_VIOLATION);
    // exception_vector_replace(ipc_exception, EXCEPTION_NUM_TRACE);

    ipc_exception_handlers[0x45] = 0x001;
    exception_vector_replace(ipc_exception, 0x45);

    struct exception_state stacked_state;  // Must exist on stack
    memcpy(&stacked_state, &thread_current->saved_state, sizeof(stacked_state));
    exception_frame_exec(&stacked_state);
    
    assert(!"This should never execute");
}
