#include "exception.h"
#include <stdio.h>

static void exception_bus_error(struct exception_state *state);
static void exception_address_error(struct exception_state *state);
static void exception_illegal_instruction(struct exception_state *state);
static void exception_zero_divide(struct exception_state *state);
static void exception_chk_instruction(struct exception_state *state);
static void exception_trapv_instruction(struct exception_state *state);
static void exception_privilege_violation(struct exception_state *state);
static void exception_trace(struct exception_state *state);
static void exception_line_1010_emulator(struct exception_state *state);
static void exception_line_1111_emulator(struct exception_state *state);
static void exception_format_error(struct exception_state *state);
static void exception_uninitialized_interrupt_vector(struct exception_state *state);
static void exception_spurious_interrupt(struct exception_state *state);

exception_func exception_table[256] = {0};

void exception_vector_raw_replace(exception_raw_func function, unsigned char vector) {
    exception_raw_func *const exception_vector_table = (exception_raw_func *) 0x00000000;
    exception_vector_table[vector] = function;
}

void exception_vector_replace(exception_func function, unsigned char vector) {
    exception_table[vector] = function;
    exception_vector_raw_replace(exception_entry, vector);
}

void exception_init(void) {
    exception_vector_replace(exception_bus_error, EXCEPTION_NUM_BUS_ERROR);
    exception_vector_replace(exception_address_error, EXCEPTION_NUM_ADDRESS_ERROR);
    exception_vector_replace(exception_illegal_instruction, EXCEPTION_NUM_ILLEGAL_INSTRUCTION);
    exception_vector_replace(exception_zero_divide, EXCEPTION_NUM_ZERO_DIVIDE);
    exception_vector_replace(exception_chk_instruction, EXCEPTION_NUM_CHK_INSTRUCTION);
    exception_vector_replace(exception_trapv_instruction, EXCEPTION_NUM_TRAPV_INSTRUCTION);
    exception_vector_replace(exception_privilege_violation, EXCEPTION_NUM_PRIVILEGE_VIOLATION);
    exception_vector_replace(exception_trace, EXCEPTION_NUM_TRACE);
    exception_vector_replace(exception_line_1010_emulator, EXCEPTION_NUM_LINE_1010_EMULATOR);
    exception_vector_replace(exception_line_1111_emulator, EXCEPTION_NUM_LINE_1111_EMULATOR);
    exception_vector_replace(exception_format_error, EXCEPTION_NUM_FORMAT_ERROR);
    exception_vector_replace(exception_uninitialized_interrupt_vector, EXCEPTION_NUM_UNINITIALIZED_INTERRUPT_VECTOR);
    exception_vector_replace(exception_spurious_interrupt, EXCEPTION_NUM_SPURIOUS_INTERRUPT);
}

void exception(struct exception_state *state) {
    unsigned short vector_number = exception_vector_number_get(state);
    if (exception_table[vector_number]) {
        exception_table[vector_number](state);
    }
}

void exception_stack_frame_print(struct exception_state *state) {
    const char *format4reg = "%4s 0x%08lX\t%4s 0x%08lX\t%4s 0x%08lX\t%4s 0x%08lX\n";
    printf(format4reg,
           "D0", (unsigned long) state->d[0],
           "D1", (unsigned long) state->d[1],
           "D2", (unsigned long) state->d[2],
           "D3", (unsigned long) state->d[3]);
    printf(format4reg,
           "D4", (unsigned long) state->d[4],
           "D5", (unsigned long) state->d[5],
           "D6", (unsigned long) state->d[6],
           "D7", (unsigned long) state->d[7]);
    printf(format4reg,
           "A0", (unsigned long) state->a[0],
           "A1", (unsigned long) state->a[1],
           "A2", (unsigned long) state->a[2],
           "A3", (unsigned long) state->a[3]);
    printf(format4reg,
           "A4", (unsigned long) state->a[4],
           "A5", (unsigned long) state->a[5],
           "A6", (unsigned long) state->a[6],
           "USP", (unsigned long) state->usp);
    printf("SR: 0x%04hX, PC: 0x%08lX, F: 0x%01hX, VO: 0x%03hX, SSP: 0x%p\n",
           (unsigned short) state->sr,
           (unsigned long) state->pc,
           (unsigned short) (state->format_vector_offset >> 12),
           (unsigned short) (state->format_vector_offset & 0xFFFU),
           (void *) (state + 1));
}

static void exception_bus_error(struct exception_state *state) {
    printf("Bus Error: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_address_error(struct exception_state *state) {
    printf("Address Error: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_illegal_instruction(struct exception_state *state) {
    printf("Illegal Instruction: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_zero_divide(struct exception_state *state) {
    printf("Zero Divide: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_chk_instruction(struct exception_state *state) {
    printf("CHK Instruction: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_trapv_instruction(struct exception_state *state) {
    printf("TRAPV Instruction: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_privilege_violation(struct exception_state *state) {
    printf("Privilege Violation: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_trace(struct exception_state *state) {
    printf("Trace: ");
    exception_stack_frame_print(state);
}

static void exception_line_1010_emulator(struct exception_state *state) {
    printf("Line 1010 Emulator: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_line_1111_emulator(struct exception_state *state) {
    printf("Line 1111 Emulator: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_format_error(struct exception_state *state) {
    printf("Format Error: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}
static void exception_uninitialized_interrupt_vector(struct exception_state *state) {
    printf("Uninitialized Interrupt Vector: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}

static void exception_spurious_interrupt(struct exception_state *state) {
    printf("Spurious Interrupt: ");
    exception_stack_frame_print(state);
    while (1)
        ;
}
