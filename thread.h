#ifndef INCLUDE_THREAD_H
#define INCLUDE_THREAD_H

#include "do_thread.h"
#include "exception.h"
#include "ipc.h"
#include "thread_id.h"
#include "assert.h" // TODO: Make <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct thread {
    // This has a fixed layout
    struct {
        union {
            ipc_info send;
            ipc_info_raw send_raw;
        };
        union {
            ipc_info recv;
            ipc_info_raw recv_raw;
        };

        ipc_info_raw version;
        
        struct exception_state saved_state;
    };

    thread_id exceptor;
    ipc_info_raw last_version;
} thread;
static_assert(
    offsetof(thread, send) == 0 &&
    offsetof(thread, recv) == 4,
    "Struct thread has incorrect structure");

extern thread *thread_current;
extern thread thread_table[THREAD_COUNT];

static inline thread_id_index thread_id_get_index(thread_id id) {
    return id >> 8;
}

static inline thread_id_version thread_id_get_version(thread_id id) {
    return id & 0xFF;
}

static inline thread_id thread_id_make(thread_id_index id_index, thread_id_version id_version) {
    if (id_version == 0) {
        return THREAD_ID_NIL;
    }
    return (id_index << 8) | (id_version & 0xFF);
}

static inline bool thread_id_matches(thread_id target, thread_id compared) {
    return compared == target || compared == THREAD_ID_WILD;
}

bool thread_get_first(thread_id_index *result);
bool thread_get_last(thread_id_index *result);
bool thread_init(thread *thrd);
thread_id thread_allocate_first(thread **result_ptr);
thread_id thread_allocate_last(thread **result_ptr);
void thread_setup(thread *thrd, void (*entry)(), void *stack_ptr, thread_id exceptor);
thread_id thread_create_first(void (*entry)(), void *stack_ptr, thread_id exceptor, thread **result_ptr);
thread_id thread_create_last(void (*entry)(), void *stack_ptr, thread_id exceptor, thread **result_ptr);

thread *thread_from_id(thread_id id);
thread *thread_from_index(thread_id_index index);
thread_id thread_to_id(thread *thrd);

bool thread_exists(thread *thrd);
bool thread_runnable(thread *thrd);

bool thread_has_permission_over(thread_id id);

void thread_copy_data(struct exception_state *dest, struct exception_state *src);
void thread_copy_state(struct exception_state *dest, struct exception_state *src);
void thread_hot_switch(thread *thrd, struct exception_state *state);
void thread_cold_switch(thread *thrd, struct exception_state *state);

void thread_schedule(struct exception_state *state);

void thread_dump_states(void);

void thread_ex_regs(struct exception_state *state);

#endif
