#include "thread.h"
#include <stddef.h>
#include <stdio.h>
#include <string.h>

thread *thread_current = NULL;
thread thread_table[THREAD_COUNT] = {0};

bool thread_get_first(thread_id_index *result) {
    for (thread_id_index i = 0; i < THREAD_COUNT; ++i) {
        if (!thread_exists(&thread_table[i])) {
            // TODO: Check bounds on version?
            *result = i;
            return true;
        }
    }
    return false;
}

bool thread_get_last(thread_id_index *result) {
    for (thread_id_index i = 0; i < THREAD_COUNT; ++i) {
        if (!thread_exists(&thread_table[THREAD_COUNT - i - 1])) {
            // TODO: Check bounds on version?
            *result = THREAD_COUNT - i - 1;
            return true;
        }
    }
    return false;
}

bool thread_init(thread *thrd) {
    // TODO: Check bounds on version?
    thread_id_version version = thrd->last_version + 1;

    thrd->send_raw = 0;
    thrd->recv_raw = 0;
    thrd->version = version;
    memset(&thrd->saved_state, 0, sizeof(thrd->saved_state));

    return true;
}

thread_id thread_allocate_first(thread **result) {
    thread_id_index index;
    if (!thread_get_first(&index)) {
        return THREAD_ID_NIL;
    }

    if (!thread_init(&thread_table[index])) {
        return THREAD_ID_NIL;
    }

    if (result) {
        *result = &thread_table[index];
    }
    return thread_id_make(index, thread_table[index].version);
}

thread_id thread_allocate_last(thread **result) {
    thread_id_index index;
    if (!thread_get_last(&index)) {
        return THREAD_ID_NIL;
    }

    if (!thread_init(&thread_table[index])) {
        return THREAD_ID_NIL;
    }

    if (result) {
        *result = &thread_table[index];
    }
    return thread_id_make(index, thread_table[index].version);
}

void thread_setup(thread *thrd, void (*entry)(), void *stack_ptr, thread_id exceptor) {
    thrd->saved_state.usp = (uintptr_t) stack_ptr;
    thrd->saved_state.sr = 0x0000;
    thrd->saved_state.pc = (uintptr_t) entry;
    thrd->saved_state.format_vector_offset = 0x0000;

    thrd->recv.id = THREAD_ID_WILD;
    thrd->recv.block = false;
    thrd->recv.error = false;

    thrd->exceptor = exceptor;
}

thread_id thread_create_first(void (*entry)(), void *stack_ptr, thread_id exceptor, thread **result) {
    thread *thrd;
    thread_id id = thread_allocate_first(&thrd);

    if (id != THREAD_ID_NIL) {
        thread_setup(thrd, entry, stack_ptr, exceptor);

        if (result) {
            *result = thrd;
        }
    }

    return id;
}

thread_id thread_create_last(void (*entry)(), void *stack_ptr, thread_id exceptor, thread **result) {
    thread *thrd;
    thread_id id = thread_allocate_last(&thrd);

    if (id != THREAD_ID_NIL) {
        thread_setup(thrd, entry, stack_ptr, exceptor);

        if (result) {
            *result = thrd;
        }
    }

    return id;
}

thread *thread_from_id(thread_id id) {
    if (id == THREAD_ID_NIL || id == THREAD_ID_WILD) {
        return NULL;
    }

    thread_id_index index = thread_id_get_index(id);
    thread_id_version version = thread_id_get_version(id);
    if (index >= THREAD_COUNT) {
        return NULL;
    }

    struct thread *thrd = &thread_table[index];
    if (!thread_exists(thrd) || thrd->version != version) {
        return NULL;
    }
    
    return thrd;
}

thread *thread_from_index(thread_id_index index) {
    if (index >= THREAD_COUNT) {
        return NULL;
    }

    struct thread *thrd = &thread_table[index];
    if (!thread_exists(thrd)) {
        return NULL;
    }

    return thrd;
}

thread_id thread_to_id(thread *thrd) {
    if (!thrd) {
        return THREAD_ID_NIL;
    }

    thread_id_index index = thrd - thread_table;
    thread_id_version version = thrd->version;
    return thread_id_make(index, version);
}

bool thread_exists(thread *thrd) {
    return thrd->version != 0;
}

bool thread_runnable(thread *thrd) {
    return thread_exists(thrd) &&
        (thrd == thread_current || (thrd->saved_state.pc && thrd->saved_state.usp)) &&
        !ipc_can_send(thrd) && !ipc_can_recv(thrd);
}

bool thread_has_permission_over(thread_id id) {
    if (id >= thread_to_id(thread_current)) {
        return true;
    } else {
        return false;
    }
}

void thread_copy_data(struct exception_state *dest, struct exception_state *src) {
    // TODO: Use memmove()
    memcpy(&dest->d, &src->d, sizeof(dest->d));
    memcpy(&dest->a, &src->a, sizeof(dest->a) - sizeof(dest->a[6]));
}

void thread_copy_state(struct exception_state *dest, struct exception_state *src) {
    dest->a[6] = src->a[6];
    dest->usp = src->usp;
    dest->sr = src->sr;
    dest->pc = src->pc;
}

void thread_hot_switch(thread *thrd, struct exception_state *state) {
    if (thrd == thread_current) {
        return;
    }

    thread_copy_state(&thread_current->saved_state, state);

    thread_copy_state(state, &thrd->saved_state);

    thread_current = thrd;
}

void thread_cold_switch(thread *thrd, struct exception_state *state) {
    if (thrd == thread_current) {
        return;
    }

    thread_copy_data(&thread_current->saved_state, state);
    thread_copy_state(&thread_current->saved_state, state);

    thread_copy_data(state, &thrd->saved_state);
    thread_copy_state(state, &thrd->saved_state);

    thread_current = thrd;
}

void thread_schedule(struct exception_state *state) {
    // printf("Schedule\n");
    // thread_dump_states();

    for (thread_id_index i = 0; i < THREAD_COUNT; ++i) {
        if (thread_runnable(&thread_table[i])) {
            // Found the highest-priority non-blocked thread
            // printf("Running thread 0x%04hX\n", thread_to_id(&thread_table[i]));
            thread_cold_switch(&thread_table[i], state);
            ipc_cleanup(state);
            return;
        }
    }

    for (thread_id_index i = 0; i < THREAD_COUNT; ++i) {
        if (thread_exists(&thread_table[i])) {
            // Found the highest-priority thread
            // printf("Force-running thread 0x%04hX\n", thread_to_id(&thread_table[i]));
            thread_cold_switch(&thread_table[i], state);
            ipc_force_unblock();
            ipc_cleanup(state);
            return;
        }
    }

    assert(!"No process could be woken");
}

void thread_dump_states(void) {
    for (thread_id_index i = 0; i < THREAD_COUNT; ++i) {
        if (thread_runnable(&thread_table[i])) {
            printf("Thread index %hX is runnable\n", i);
        } else {
            printf("Thread index %hX is busy: v=%04hX s=%08lX r=%08lX\n", i, thread_table[i].version, thread_table[i].send_raw, thread_table[i].recv_raw);
        }
    }
}

void thread_ex_regs(struct exception_state *state) {
    asm ("ori #(7 << 8), %SR");

    thread_id id = state->d[0];
    uintptr_t pc = state->d[1];
    uintptr_t usp = state->d[2];
    uint8_t ccr = state->d[3];
        
    if (!thread_has_permission_over(id)) {
        state->d[0] = THREAD_ID_NIL;
        return;
    }

    thread *thrd = thread_from_id(id);
    if (!thrd) {
        state->d[0] = THREAD_ID_NIL;
        return;
    }

    uintptr_t old_pc = thrd->saved_state.pc;
    uintptr_t old_usp = thrd->saved_state.usp;
    uint8_t old_ccr = thrd->saved_state.sr & 0xFF;

    if (pc && usp) {
        thrd->saved_state.pc = pc;
        thrd->saved_state.usp = usp;
        thrd->saved_state.sr &= ~0xFF;
        thrd->saved_state.sr |= ccr;
    }
    
    state->d[1] = old_pc;
    state->d[2] = old_usp;
    state->d[3] = old_ccr;
}
