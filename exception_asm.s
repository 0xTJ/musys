    export  exception_entry
    export  exception_leave
    export  exception_frame_exec
    import  exception
    
    section .text

exception_entry:
    sub.l   #4,SP   ; Leave space for USP
    movem.l D0-D7/A0-A6,-(SP)
    move.l  USP,A1
    move.l  A1,(60,SP)

    ; Add pointer to full exception frame onto stack before calling exception()
    move.l  SP,-(SP)
    jsr     exception
    add.l   #4,SP

exception_leave:
    move.l  (60,SP),A1
    move.l  A1,USP
    movem.l (SP)+,D0-D7/A0-A6
    add.l   #4,SP   ; Remove space for USP
    rte

exception_frame_exec:
    move.l  (4,SP),SP
    jmp     exception_leave
