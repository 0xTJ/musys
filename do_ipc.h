#ifndef INCLUDE_DO_IPC_H
#define INCLUDE_DO_IPC_H

#include "thread_id.h"
#include "assert.h" // TODO: Make <assert.h>
#include <stdbool.h>
#include <stdint.h>

#define IPC_ERROR_SHIFT 0
#define IPC_ERROR_WIDTH 1
#define IPC_ERROR_VMASK ((1UL << IPC_ERROR_WIDTH) - 1)
#define IPC_ERROR_MASK (IPC_ERROR_VMASK << IPC_ERROR_SHIFT)
#define IPC_ERROR_END (IPC_ERROR_SHIFT + IPC_ERROR_WIDTH)

#define IPC_PADDING_SHIFT IPC_ERROR_END
#define IPC_PADDING_WIDTH 13
#define IPC_PADDING_VMASK ((1UL << IPC_PADDING_WIDTH) - 1)
#define IPC_PADDING_MASK (IPC_PADDING_VMASK << IPC_PADDING_SHIFT)
#define IPC_PADDING_END (IPC_PADDING_SHIFT + IPC_PADDING_WIDTH)

#define IPC_HAS_STRING_SHIFT IPC_PADDING_END
#define IPC_HAS_STRING_WIDTH 1
#define IPC_HAS_STRING_VMASK ((1UL << IPC_HAS_STRING_WIDTH) - 1)
#define IPC_HAS_STRING_MASK (IPC_HAS_STRING_VMASK << IPC_HAS_STRING_SHIFT)
#define IPC_HAS_STRING_END (IPC_HAS_STRING_SHIFT + IPC_HAS_STRING_WIDTH)

#define IPC_BLOCK_SHIFT IPC_HAS_STRING_END
#define IPC_BLOCK_WIDTH 1
#define IPC_BLOCK_VMASK ((1UL << IPC_BLOCK_WIDTH) - 1)
#define IPC_BLOCK_MASK (IPC_BLOCK_VMASK << IPC_BLOCK_SHIFT)
#define IPC_BLOCK_END (IPC_BLOCK_SHIFT + IPC_BLOCK_WIDTH)

#define IPC_ID_SHIFT IPC_BLOCK_END
#define IPC_ID_WIDTH 16
#define IPC_ID_VMASK ((1UL << IPC_ID_WIDTH) - 1)
#define IPC_ID_MASK (IPC_ID_VMASK << IPC_ID_SHIFT)
#define IPC_ID_END (IPC_ID_SHIFT + IPC_ID_WIDTH)

#define IPC_TRAP_NUM 0
#define IPC_VECTOR_NUM (0x20 + IPC_TRAP_NUM)

#define IPC_STRINGS_MAX 16

typedef uint32_t ipc_info_raw;

typedef union ipc_info {
    struct {
        ipc_info_raw id : IPC_ID_WIDTH;
        ipc_info_raw block : IPC_BLOCK_WIDTH;
        ipc_info_raw has_string : IPC_HAS_STRING_WIDTH;
        ipc_info_raw padding : IPC_PADDING_WIDTH;
        ipc_info_raw error : IPC_ERROR_WIDTH;
    };
    ipc_info_raw raw;
} ipc_info;
static_assert(sizeof(ipc_info) == sizeof(uint32_t), "Union ipc_info has incorrect size");

typedef struct ipc_info_pair {
    ipc_info send;
    ipc_info recv;
} ipc_info_pair;
static_assert(sizeof(ipc_info_pair) == sizeof(uint64_t), "Union ipc_info_pair has incorrect size");

typedef struct ipc_string {
    void *addr;
    size_t size;
} ipc_string;

static inline ipc_info_pair do_ipc(ipc_info_raw send, ipc_info_raw recv,
                                   uint32_t data[8],
                                   ipc_string *send_strings, ipc_string *recv_strings) {
    register ipc_info_raw send_reg asm ("d0") = send;
    register ipc_info_raw recv_reg asm ("d1") = recv;
    register uint32_t data_0_reg asm("d2") = data[0];
    register uint32_t data_1_reg asm("d3") = data[1];
    register uint32_t data_2_reg asm("d4") = data[2];
    register uint32_t data_3_reg asm("d5") = data[3];
    register uint32_t data_4_reg asm("d6") = data[4];
    register uint32_t data_5_reg asm("d7") = data[5];
    register uint32_t data_6_reg asm("a0") = data[6];
    register uint32_t data_7_reg asm("a1") = data[7];
    register ipc_string *send_strings_reg asm("a2") = send_strings;
    register ipc_string *recv_strings_reg asm("a3") = recv_strings;

    asm volatile (
        "trap    %[trap_num] \n"
        : "+r" (send_reg), "+r" (recv_reg),
          "+r" (data_0_reg), "+r" (data_1_reg),
          "+r" (data_2_reg), "+r" (data_3_reg),
          "+r" (data_4_reg), "+r" (data_5_reg),
          "+r" (data_6_reg), "+r" (data_7_reg),
          "+r" (send_strings_reg), "+r" (recv_strings_reg)
        : [trap_num] "Ci" (IPC_TRAP_NUM)
        : "a4", "a5", "cc", "memory"
    );

    data[0] = data_0_reg;
    data[1] = data_1_reg;
    data[2] = data_2_reg;
    data[3] = data_3_reg;
    data[4] = data_4_reg;
    data[5] = data_5_reg;
    data[6] = data_6_reg;
    data[7] = data_7_reg;

    ipc_info_pair result = { .send = { .raw = send_reg }, .recv = { .raw = recv_reg } };
    return result;
}

static inline ipc_info_pair do_ipc_generic(thread_id send_id, bool send_block,
                                           thread_id recv_id, bool recv_block,
                                           uint32_t data[8],
                                           ipc_string *send_strings, ipc_string *recv_strings) {
    ipc_info send = {
        .id = send_id,
        .has_string = (bool) send_strings,
        .block = send_block,
    };
    ipc_info recv = {
        .id = recv_id,
        .has_string = (bool) recv_strings,
        .block = recv_block,
    };

    return do_ipc(send.raw, recv.raw, data, send_strings, recv_strings);
}

static inline ipc_info do_ipc_send(thread_id id, bool block,
                                   uint32_t data[8],
                                   ipc_string *strings) {
    return do_ipc_generic(id, block, THREAD_ID_NIL, false, data, strings, NULL).send;
}

static inline ipc_info do_ipc_recv(thread_id id, bool block,
                                   uint32_t data[8],
                                   ipc_string *strings) {
    return do_ipc_generic(THREAD_ID_NIL, false, id, block, data, NULL, strings).recv;
}

static inline ipc_info do_ipc_wait(uint32_t data[8],
                                   ipc_string *strings) {
    return do_ipc_generic(THREAD_ID_NIL, false, THREAD_ID_WILD, true, data, NULL, strings).recv;
}

static inline ipc_info_pair do_ipc_call(thread_id id, bool block,
                                        uint32_t data[8],
                                        ipc_string *send_strings,
                                        ipc_string *recv_strings) {
    return do_ipc_generic(id, block, id, true, data, send_strings, recv_strings);
}

static inline ipc_info_pair do_ipc_reply_and_wait(thread_id id, bool block,
                                                  uint32_t data[8],
                                                  ipc_string *send_strings,
                                                  ipc_string *recv_strings) {
    return do_ipc_generic(id, block, THREAD_ID_WILD, true, data, send_strings, recv_strings);
}

#endif
