#ifndef INCLUDE_THREAD_ID_H
#define INCLUDE_THREAD_ID_H

#include "exception.h"
#include "assert.h" // TODO: Make <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define THREAD_COUNT 32

typedef uint16_t thread_id;
typedef thread_id thread_id_index;
typedef thread_id thread_id_version;
struct thread;

#define THREAD_ID_NIL (((thread_id) 0) & IPC_ID_VMASK)
#define THREAD_ID_WILD (((thread_id) -1) & IPC_ID_VMASK)
#define THREAD_ID_EXCP (((thread_id) -2) & IPC_ID_VMASK)

#include "do_ipc.h"

#endif
