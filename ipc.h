#ifndef INCLUDE_IPC_H
#define INCLUDE_IPC_H

#include "do_ipc.h"
#include "exception.h"
#include "thread.h"
#include "thread_id.h"
#include "assert.h" // TODO: Make <assert.h>
#include <stdbool.h>
#include <stdint.h>

extern thread_id ipc_exception_handlers[EXCEPTION_COUNT];

void ipc_assert_layout(void);

void ipc_strings_copy(struct thread *dest_thrd, ipc_string *dest_strings, struct thread *src_thrd, ipc_string *src_strings);

bool ipc_can_send(struct thread *thrd);
bool ipc_can_recv(struct thread *thrd);

void ipc_force_unblock(void);
void ipc_cleanup(struct exception_state *state);

void ipc(struct exception_state *state);

void ipc_exception(struct exception_state *state);
bool ipc_deliver_exception(struct exception_state *state);

#endif
