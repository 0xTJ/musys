#ifndef INCLUDE_EXCEPTION_H
#define INCLUDE_EXCEPTION_H

#include <stdint.h>
#include <stdnoreturn.h>

#define EXCEPTION_COUNT 0x100

#define EXCEPTION_NUM_BUS_ERROR 0x02
#define EXCEPTION_NUM_ADDRESS_ERROR 0x03
#define EXCEPTION_NUM_ILLEGAL_INSTRUCTION 0x04
#define EXCEPTION_NUM_ZERO_DIVIDE 0x05
#define EXCEPTION_NUM_CHK_INSTRUCTION 0x06
#define EXCEPTION_NUM_TRAPV_INSTRUCTION 0x07
#define EXCEPTION_NUM_PRIVILEGE_VIOLATION 0x08
#define EXCEPTION_NUM_TRACE 0x09
#define EXCEPTION_NUM_LINE_1010_EMULATOR 0x0A
#define EXCEPTION_NUM_LINE_1111_EMULATOR 0x0B
#define EXCEPTION_NUM_FORMAT_ERROR 0x0E
#define EXCEPTION_NUM_UNINITIALIZED_INTERRUPT_VECTOR 0x0F
#define EXCEPTION_NUM_SPURIOUS_INTERRUPT 0x18
// TODO: Add others

struct exception_state {
    uint32_t d[8];
    uint32_t a[7];
    uint32_t usp;
    uint16_t sr;
    uint32_t pc;
    uint16_t format_vector_offset;
} __attribute__((packed));

typedef void (*exception_raw_func)(void);
typedef void (*exception_func)(struct exception_state *state);

// These two functions should not be called from C
// They don't expect a return address on the stack
void exception_entry(void);
void exception_leave(void);

// Sets stack pointer to state, then jumps to exception_leave()
noreturn void exception_frame_exec(struct exception_state *state);

void exception_vector_raw_replace(exception_raw_func function, unsigned char vector);
void exception_vector_replace(exception_func function, unsigned char vector);

void exception_init(void);
void exception(struct exception_state *state);
void exception_stack_frame_print(struct exception_state *state);

static inline uint8_t exception_vector_number_get(const struct exception_state *state) {
    uint16_t vector_offset = state->format_vector_offset & 0xFFF;
    uint8_t vector_number = vector_offset >> 2;
    return vector_number;
}

#endif
