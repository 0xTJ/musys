#include "assert.h" // TODO: Make <assert.h>

#include <stdio.h>

void __assert_fail(const char *__expression, const char *__file, unsigned long __line, const char *__func) {
    printf("%s:%lu: %s: Assertion `%s' failed.", __file, __line, __func, __expression);
    asm ("ori #(7 << 8), %SR");
    while(1) {
        // Loop forever
    }
}
