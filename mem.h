#ifndef INCLUDE_MEM_H
#define INCLUDE_MEM_H

#include "thread.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

typedef struct {
    size_t val;
    bool exists;
} opt_size_t;

typedef struct {
    void *val;
    bool exists;
} opt_ptr;

static inline opt_size_t mem_get_size_t(thread *thrd, size_t *ptr) {
    return (opt_size_t) { *ptr, true };
}

static inline bool mem_set_size_t(thread *thrd, size_t *ptr, size_t val) {
    *ptr = val;
    return true;
}

static inline opt_ptr mem_get_ptr(thread *thrd, void **ptr) {
    return (opt_ptr) { *ptr, true };
}

static inline bool mem_set_ptr(thread *thrd, void **ptr, void *val) {
    *ptr = val;
    return true;
}

// TODO: This is unsafe
static inline opt_ptr mem_get_ptr_alias(thread *thrd, void *ptr) {
    return mem_get_ptr(thrd, ptr);
}

// TODO: This is unsafe
static inline bool mem_set_ptr_alias(thread *thrd, void *ptr, void *val) {
    return mem_set_ptr(thrd, (void **) ptr, val);
}

static inline void mem_copy(thread *dest_thrd, void *dest, thread *src_thrd, const void *src, size_t count) {
    memcpy(dest, src, count);
}

#endif
