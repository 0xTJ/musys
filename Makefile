THISDIR ?= .
ROSCODIR ?= $(THISDIR)/../../..
SYSDIR ?= $(ROSCODIR)/code/software/libs/build
SYSINCDIR ?= ${SYSDIR}/include
SYSLIBDIR ?= ${SYSDIR}/lib
GCC_LIBS = \
	$(shell $(CC) --print-search-dirs	\
	| grep libraries:\ =				\
	| sed 's/libraries: =/-L/g'			\
	| sed 's/:/m68000\/ -L/g')m68000/

TARGET ?= musys.bin
OBJS := \
	kmain.o assert.o					\
	exception.o exception_asm.o			\
	ipc.o								\
	thread.o							\

LDSCRIPT ?= musys.ld

LIBS = -lprintf -lcstdlib -lmachine -lstart_serial -lgcc
DEFINES = -DROSCO_M68K

CFLAGS = \
	-std=gnu11 -ffreestanding -Wall -pedantic -Werror			\
	-I $(SYSINCDIR)	-I $(THISDIR)								\
	-mcpu=68010 -march=68010 -mtune=68010						\
	$(DEFINES) -O2 -g -MD -MP

LDFLAGS = \
	-T $(LDSCRIPT) -L $(SYSLIBDIR)	 \
	-Map=$(MAP) --pic-executable -N

ASFLAGS = -Felf -m68010 -quiet $(DEFINES) -dependall=make -depfile $(@:.o=.d)

CC = m68k-elf-gcc
LD = m68k-elf-ld
AR = m68k-elf-ar
RANLIB = m68k-elf-ranlib
AS = vasmm68k_mot
RM_F = rm -f
RM_RF = $(RM_F) -r
MKDIR_P = mkdir -p
CP_R = cp -r

DEPS := $(OBJS:.o=.d)
MAP = $(TARGET:.bin=.map)

$(TARGET): $(OBJS) | $(LDSCRIPT)
	$(LD) $(LDFLAGS) $(GCC_LIBS) $^ -o $@ $(LIBS)
	chmod a-x $@

.PHONY: all clean

all: $(TARGET)

clean: 
	$(RM_F) $(OBJS) $(DEPS) $(TARGET) $(MAP)

-include $(DEPS)
